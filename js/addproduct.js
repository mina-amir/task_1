$(document).ready(function () {

    $('#productType').change(function () {

        $('#productType > option').each(function() {
           var id= $(this).text();
            $('.'+id).prop('required',false);
            $('#'+id).hide();
        });
        var type=$(this).find(':selected')[0].value;
        $('.'+type).prop('required',true);
        $('#'+type).show();
    });


});