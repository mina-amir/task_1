<?php


abstract class main
{
    protected $name;
    protected $sku;
    protected $price;
    protected $description;

    public function setName($name){return $this->name =$name;}
    public function setSKU($sku){return $this->sku =$sku;}
    public function setPrice($price){return $this->price =$price;}
    abstract protected function setDescription();

    public function getName(){return $this->name;}
    public function getSKU(){return $this->sku;}
    public function getPrice(){return $this->price;}
    public function getDescription(){return $this->description;}

    public function add_product(){
        $this->setDescription();
        if (isset($this->name)&&isset($this->sku)&&isset($this->description)&&isset($this->price))
        {
            $con= new PDO("mysql:host=".Servername.";dbname=".db_name,username,password);
            $query=$con->prepare("INSERT INTO `products`(`sku`,`name`,`price`,`description`)VALUES (?,?,?,?)");
            $query->bindParam(1,$this->sku,PDO::PARAM_STR);
            $query->bindParam(2,$this->name,PDO::PARAM_STR);
            $query->bindParam(3,$this->price,PDO::PARAM_INT);
            $query->bindParam(4,$this->description,PDO::PARAM_STR);
            if($query->execute()){return true;}
            return false;
        }
        return false;
    }

}

class DVD extends main
{

    protected function setDescription(){
         //get DVD data in MB from the submitted form
         $size = $_POST['size'];
         //set description for adding into db as "Size : size MB"
         return $this->description = "Size : " . $size . " MB";
    }
}

class Book extends main
{
    protected function setDescription(){
        //get book data in weight from the submitted form
        $weight = $_POST['weight'];
        //set description for adding into db as "Weight : weight KG"
        return $this->description = "Weight : " . $weight . " KG";
    }
}

class Furniture extends main
{
    protected function setDescription(){
        //get furniture data in H W L from the submitted form
        $H = $_POST['height'];
        $W = $_POST['width'];
        $L = $_POST['length'];
        //set description for adding into db as "Dimensions : HxWxL"
        return $this->description = "Dimensions : " . $H . "x" . $W . "x" . $L;
    }
}





