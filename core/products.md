﻿# Products class Doc

**products** class is that contains four main methods that helps you to deal with all your needs and handle the data from-to database in object form.

# Properties
There are mainly four properties in product class. 
'name' property is to define your product name.
'sku' property is to define your product SKU.
' price' property is to define your product price.
'desc' is the last property that is handled automatically by the algorithm provided in the class. 
**Note: setting the 'desc' value will be ignored**
# set( key , value ) 

**set** is setter for your main properties of your product object.
*i.e  `set( 'name'  ,  'Scandiweb');`*

# get ( key )
**get** is the getter for your main properties of your product object.
Returns the value of given key.
*i.e  `get( 'name');  //output scandiweb`*
# add_product()

It is the adding method for your new product that mainly sets the 'desc' property depending on your product type automatically and call a function from the main logic class that deals with the database creating connection and inserting your data as an object into your table.
Returns true if successful and false if not.
**Note: All prooerties must be setted before calling add_product method.**
*i.e` $obj= new product();` 
`$obj->set('sku, ' 000-000-000' );`
`$obj->set('name', ' scandiweb' );`
`$obj->set('price', ' 1000' );`
`$obj->add_product();`*

## get_products()

The get all method for the class. It is the main display method that returns an array of objects with all the data inserted into the database.
Returns array of objects indexed with product id. 
` $obj= new product();`
`$data=$obj->get_products();`

## delete_products( array )

Delete one or more product with this method. It takes one array argument with the values of the selected products IDs. It loops for it and delete each poroduct within the array.
Returns true if successful and false if not.
` $obj= new product();`
`$data=$obj->delete_products( array( 1, 2 ,3 ) );`

## DVD()
It is a protected method that sets the 'desc' property with the submitted data for the DVD product type.
*Called in the add_product() method*


# Book()


It is a protected method that sets the 'desc' property with the submitted data for the Book product type.
*Called in the add_product() method*

## Furniture()

It is a protected method that sets the 'desc' property with the submitted data for the Furniture product type.
*Called in the add_product() method*

## Full implementaion example

    //adding new product
    
    $product = new prodcuts();
    $product->set('sku', $_POST['sku'] );
    $product->set('name', $_POST['name'] );
    $product->set('price', $_POST['price'] );
    $product->add_product();
    
    //getting all products
    
    $products=new products();
    $all_products = $products->get_products();
    foreach($all_products as $id => $product)
    {
	    //your code
    }
    
    
    //delete multiple products 
    $del=new products();
    //delete products with IDs 1,2 and 3
    $del->delete_products( array( 1,2,3 ) ); 

## UML diagrams

```mermaid
classDiagram
 Main --|> Products : Extend
```


Products class:

```mermaid
graph LR
A[add_product] -- Call --> B(Book)
A -- Call --> C(Furniture)
A -- Call --> D(DVD)
D --> E{main::Insert}
B --> E
C -->E

AA[delete_products]-- Call --> BB{main::delete}

AAA[get_products]-- Call --> BBB{main::getall}
```

