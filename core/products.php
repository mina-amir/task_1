<?php

require_once "main.php";
class products extends main
{
    private $className;
    private $id;
    public function setName($name)
    {
        if(!is_object($this->className))
        {
            $type = $_POST['type'];
            $this->className =new $type();
        }
        return $this->className->setName($name);

    }
    public function setSKU($sku)
    {
        if(!is_object($this->className))
        {
            $type = $_POST['type'];
            $this->className =new $type();
        }
        return $this->className->setSKU($sku);

    }
    public function setPrice($price)
    {
        if(!is_object($this->className))
        {
            $type = $_POST['type'];
            $this->className =new $type();
        }
        return $this->className->setPrice($price);

    }
    protected function setDescription()
    {
        if(!is_object($this->className))
        {
            $type = $_POST['type'];
            $this->className =new $type();
        }
        return $this->className->setDescription();

    }
    public function getID(){return $this->id;}

    public function add_product()
    {
        if(!is_object($this->className))
        {
            $type = $_POST['type'];
            $this->className =new $type();
        }
        return $this->className->add_product();

    }

    public function delete_products($selected = array())
    {
        $con= new PDO("mysql:host=".Servername.";dbname=".db_name,username,password);
        $query=$con->prepare("DELETE FROM `products` WHERE `id`=?");
        foreach ($selected as $id) {
            $query->bindParam(1, $id, PDO::PARAM_INT);
            $query->execute();
            if ($query->rowCount()==0){return false;}
        }
        return true;
    }

    public function get_products()
    {
        $data=array();
        $con= new PDO("mysql:host=".Servername.";dbname=".db_name,username,password);
        $query=$con->prepare("SELECT * FROM `products`");
        $query->setFetchMode(PDO::FETCH_CLASS,"products");
        $query->execute();
        while($row = $query->fetch())
        {
            $data[]=$row;
        }
        return $data;
    }
}
