<?php

require "core/config.php";
require "core/products.php";

//if selected data submitted for deletion
if (isset($_POST['selected']))
{
    //get the selected array
    $delete_array =$_POST['selected'];
    $delete_products= new products();
    $delete_products->delete_products($delete_array);
}
//get all products data to view
$products =new products();
$all_products= $products->get_products();

include "front/index.html";